ACCOUNTID=128842882846
REGION=ap-southeast-1
SERVICEROLE=arn:aws:iam::${ACCOUNTID}:role/iam-batch-awsBatchServiceRole-X3W1DA377SAA
IAMFLEETROLE=arn:aws:iam::${ACCOUNTID}:role/iam-batch-spotFleetRole-1TH7BWYJV86HZ
INSTANCEROLE=arn:aws:iam::${ACCOUNTID}:instance-profile/iam-batch-ecsInstanceRole-14BSM996NF68T
JOBROLEARN=arn:aws:iam::${ACCOUNTID}:role/iam-batch-ecsTaskRole-OTS48XL2V2U8

SUBNETS=subnet-5ff0ab19,subnet-577ae032,subnet-492da23e
SECGROUPS=sg-57097b32,sg-ab3251d2
IMAGEID=ami-01795f7d
KEYPNAME=pipeline
MINCPU=0
MAXCPU=256
DESIREDCPU=0
RETRIES=1

TOOLS=$1
REGISTRY=${ACCOUNTID}.dkr.ecr.${REGION}.amazonaws.com
TOOLSIMAGE=${REGISTRY}/${TOOLS}
TOOLSCPU=32
TOOLSMEM=80000


ENV=${TOOLS}

# Be sure to escape SubnetIds and SecurityGroupIds as they require a basestring and not a list
aws cloudformation create-stack --stack-name batch-genomics-${ENV} --template-body file://aws-batch-genomics/batch/setup/batch_env.template.yaml --parameters \
ParameterKey=BatchServiceRole,ParameterValue=${SERVICEROLE} \
ParameterKey=SpotIamFleetRole,ParameterValue=${IAMFLEETROLE} \
ParameterKey=InstanceRole,ParameterValue=${INSTANCEROLE} \
ParameterKey=JobRoleArn,ParameterValue=${JOBROLEARN} \
ParameterKey=SubnetIds,ParameterValue=\"${SUBNETS}\" \
ParameterKey=SecurityGroupIds,ParameterValue=\"${SECGROUPS}\" \
ParameterKey=ImageId,ParameterValue=${IMAGEID} \
ParameterKey=KeyPair,ParameterValue=${KEYPNAME} \
ParameterKey=MinvCpus,ParameterValue=${MINCPU} \
ParameterKey=DesiredvCpus,ParameterValue=${DESIREDCPU} \
ParameterKey=MaxvCpus,ParameterValue=${MAXCPU} \
ParameterKey=Env,ParameterValue=${ENV} \
ParameterKey=RetryNumber,ParameterValue=${RETRIES} \
ParameterKey=ToolsDockerImage,ParameterValue=${TOOLSIMAGE} \
ParameterKey=ToolsVcpus,ParameterValue=${TOOLSCPU} \
ParameterKey=ToolsMemory,ParameterValue=${TOOLSMEM}


export BATCH_FILE_TYPE="script"

export BATCH_FILE_S3_URL="s3://testbatch-dks/myjob.sh"

aws batch submit-job --job-name test_tools --job-queue highPriority-${ENV} --job-definition ${TOOLS}-${ENV}:1 --container-overrides '{
"command": ["myjob.sh,60"]
}'